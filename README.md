# DTNDraw - simple draw tool for everyone

- Written in [VanilaJS](http://vanilla-js.com/).
- Rewritten in [Typescript](https://www.typescriptlang.org/)
- Url:
  [https://taiprogramer.github.io/dtndraw](https://taiprogramer.github.io/dtndraw)
- Status: **Latest::v0.3.2**
- Author: taiprogramer.

## Build from source

1. Install `typescript` compiler.

```sh
yarn global add typescript
```

2. Build

```sh
chmod +x build.sh && ./build.sh
```

Open `index.html` in **docs/** to enjoy.
